import {Paddle} from 'BreakoutGame';

describe("Paddle", () => {
    let paddle: Paddle;
    let game: Phaser.Game;

    beforeEach(() => {
        paddle = new Paddle(game, 0, 0);
    });

    it("moves the paddle x position 500 when sent a new location of 500", () => {
        let expected = 500;

        paddle.setX(500);

        let actual = paddle.body.x;
        expect(actual).toEqual(expected);
    });

    it("will set paddle x position to 0 when given a new location of -100", () => {
        let expected = 0;

        paddle.setX(-100);

        let actual = paddle.body.x;
        expect(actual).toEqual(expected);
    });

    it("will not set paddle x position beyond right edge of screen when given a new location of 10000", () => {
        let expected = game.width;

        paddle.setX(10000);

        let actual = paddle.body.x;
        expect(actual).toBeLessThan(expected);
    });
})