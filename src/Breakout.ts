export module Breakout {
	export class Breakout {
		public game: Phaser.Game;
		private gameWindowWidth: number = 960;
		private gameWindowHeight: number = 960;
		private gameDOM: string = 'game';

		constructor() {
			this.game = new Phaser.Game(this.gameWindowWidth, this.gameWindowHeight, Phaser.AUTO, this.gameDOM, {
				preload: this.preload,
				create: this.create
			});
		}

		private preload() {
			this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
			this.game.scale.pageAlignHorizontally = true;
			this.game.scale.pageAlignVertically = true;

			let graphicsAssetFolder: string = "./assets/graphics/";
			let graphicsStyleFolder: string = "classic/";

			let assetLocation = `${graphicsAssetFolder}${graphicsStyleFolder}`;

			this.game.load.image('paddle', assetLocation + 'paddle.png');
			this.game.load.image('ball', assetLocation + 'ball.png');
			this.game.load.image('brick', assetLocation + 'brick.png');

			this.game.load.json('messages', './assets/text/messages.json');
		}

		private create() {
			this.game.state.add('GamePlayState', BreakoutGame.GamePlayState, true);
			this.game.state.add('GameOverState', BreakoutGame.GameOverState, false);
		}
	}
}