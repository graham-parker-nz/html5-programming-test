module BreakoutGame {
    export class Brick extends Phaser.Sprite {
        constructor(game: Phaser.Game, x: number, y: number) {
            super(game, x, y, "brick");

            this.anchor.set(0, 0);

            this.game.add.existing(this);

            this.body.immovable = true;
        }

        public setSizeOfBrick(this, gameRowCount: number, gameColCount: number) {
            this.width = this.game.width / gameRowCount;
            this.height = this.game.height / gameColCount;
        }
    }
}