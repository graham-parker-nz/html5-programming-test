module BreakoutGame {
    export class Ball extends Phaser.Sprite {
        private velocityX = 400;
        private velocityY = 400;

        constructor(game: Phaser.Game, x: number, y: number) {
            super(game, x, y, "ball");

            this.anchor.set(0.5, 0.5);

            this.game.add.existing(this);

            this.body.collideWorldBounds = true;
            this.body.bounce.setTo(1); 
            this.checkWorldBounds = true;

            this.body.velocity.set(this.velocityX, this.velocityY);
        }
    }
}