module BreakoutGame {
    export class Paddle extends Phaser.Sprite {

        constructor(game: Phaser.Game, x: number, y: number) {
            super(game, x, y, "paddle");

            this.anchor.set(0.5, 0);

            this.game.add.existing(this);

            this.body.immovable = true;
            this.body.collideWorldBounds = true;
        }

        private calcFurthestRightPaddleShouldBe() {
            let paddleWidth = this.body.width;

            return this.game.width - paddleWidth;
        }

        private getPositionWithinScreenBoundaries(x: number) {
            let minBoundary = 0;
            let maxBoundary = this.calcFurthestRightPaddleShouldBe();

            return (Math.min(maxBoundary, Math.max(minBoundary, x)));
        }

        public setVelocity(velocity: number) {
            this.body.velocity.x = velocity;
        }

        public setX(x: number) {
            this.body.x = this.getPositionWithinScreenBoundaries(x);
        }

        public getCenter() {
            return this.body.width / 2;
        }
    }
}