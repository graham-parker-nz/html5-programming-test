module BreakoutGame {
    export enum PaddleVelocity {
        Left = -600,
        Right = 600,
        Idle = 0
    }
}