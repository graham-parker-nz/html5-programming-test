module BreakoutGame {
    export class GameOverState extends Phaser.State {
        private messageToShow: string;

        init(message: string) {
            this.messageToShow = message;
            this.create;
        }

        public static preload(load: Phaser.Loader): void {
        }

        restartRequested() {
            this.game.state.start("GamePlayState");
        }
    
        public create() {
            this.input.onTap.addOnce(this.restartRequested, this);

            let message = this.game.cache.getJSON('messages')[this.messageToShow];
            console.log(this.messageToShow);
            let style = { font: "6em Arial", fill: "#fff" }
            let text = this.game.add.text(this.game.world.centerX, this.game.world.centerY, message, style);
            text.anchor.setTo(0.5);
        }

        public update() {
        }
    }
}