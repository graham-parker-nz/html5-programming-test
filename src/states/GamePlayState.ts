module BreakoutGame {
    export class GamePlayState extends Phaser.State {
        paddle: BreakoutGame.Paddle;
        ball: BreakoutGame.Ball;
        brick: BreakoutGame.Brick;
        bricks: Phaser.Group;
        private LEFT_INPUT: Phaser.Key;
        private RIGHT_INPUT: Phaser.Key;
        private mouseInputInsteadOfTouch: boolean = true;    // To be a toggle in game settings
        private brickColCount: number = 14;
        private brickRowCount: number = 6;
        public gameColCount: number = 16;
        private gameRowCount: number = 28;
        private rowCountAboveBricks: number = 4;
        private brickTints = [0xc84848, 0xc66c3a, 0xb47a30, 0xa2a22a, 0x48a048, 0x4248c8];  // Ideally not hard coded

        public static preload(load: Phaser.Loader): void {
        }

        private createKeyListeners() {
            this.LEFT_INPUT = this.game.input.keyboard.addKey(Phaser.Keyboard.LEFT);
            this.RIGHT_INPUT = this.game.input.keyboard.addKey(Phaser.Keyboard.RIGHT);
        }

        private createPaddle() {
            this.paddle = new Paddle(this.game, this.game.width / 2, this.game.height - 50);
        }

        private createBall() {
            this.ball = new Ball(this.game, this.game.width / 2, 350);
            this.ball.events.onOutOfBounds.add(this.ballOutOfBounds, this);
        }

        private calcXPosToCenterBricks() {
            let brickPixelWidth = (this.game.width / this.gameColCount);
            let colCountWithNoBricks = this.gameColCount - this.brickColCount;
            let colCountEitherSideOfBricksWithNoBricks = colCountWithNoBricks / 2;
            let pixelsOnSideWithNoBricks = colCountEitherSideOfBricksWithNoBricks * brickPixelWidth;
            let quarterOfBrickPixels = brickPixelWidth / 4;

            return pixelsOnSideWithNoBricks - quarterOfBrickPixels;
        }

        private calcYPosToBricks() {
            let brickPixelHeight = (this.game.height / this.gameRowCount);

            return brickPixelHeight * this.rowCountAboveBricks;
        }

        private createBricks() {
            this.bricks = this.game.add.group();

            let brickWidth = (this.game.width / this.gameColCount);
            let brickHeight = (this.game.height / this.gameRowCount);
            let brickXStart = this.calcXPosToCenterBricks();
            let brickYStart = this.calcYPosToBricks();

            for (let row = 0; row < this.brickRowCount; row++) {
                for (let col = 0; col < this.brickColCount; col++) {
                    let x = brickXStart + (col * brickWidth);
                    let y = brickYStart + (row * brickHeight);
                    let brick = new Brick(this.game, x, y);

                    brick.setSizeOfBrick(this.gameColCount, this.gameRowCount);
                    brick.tint = this.brickTints[row];

                    this.bricks.add(brick);                
                }
            }
        }
    
        public create() {
            this.game.physics.startSystem(Phaser.Physics.ARCADE);
            this.game.world.enableBody = true;
            this.game.physics.arcade.checkCollision.down = false;

            this.createPaddle();
            this.createBall();
            this.createBricks();

            this.createKeyListeners();
        }

        private checkKeyListeners() {
            if (this.LEFT_INPUT.isDown) {
                this.paddle.setVelocity(PaddleVelocity["Left"])
            } else if (this.RIGHT_INPUT.isDown) {
                this.paddle.setVelocity(PaddleVelocity["Right"])
            } else {
                this.paddle.setVelocity(PaddleVelocity["Idle"])
            }
        }

        private checkMouseMovement() {
            this.paddle.setX(this.game.input.mousePointer.x - this.paddle.getCenter());
        }

        private checkTouchMovement() {
            if (this.game.input.pointer1.isDown) {
                this.paddle.setX(this.game.input.x - this.paddle.getCenter());
            }
        }

        private checkForUserInput() {
            if (this.mouseInputInsteadOfTouch) this.checkMouseMovement();
            else this.checkTouchMovement();

            this.checkKeyListeners();
        }

        private removeBrick(ball, brick) {
            brick.kill();
        }

        private checkForCollisions() {
            this.game.physics.arcade.collide(this.paddle, this.ball);
            this.game.physics.arcade.collide(this.ball, this.bricks, this.removeBrick, null, this);
        }

        private ballOutOfBounds(ball) {
            this.game.state.start("GameOverState", false, false, 'try-again');
        }

        private won() {
            this.game.state.start("GameOverState", true, false, 'win');
        }

        private checkWinCondition() {
            if (this.bricks.countLiving() === 0) this.won();
        }

        public update() {
            this.checkForUserInput();
            this.checkForCollisions();
            this.checkWinCondition();
        }
    }
}